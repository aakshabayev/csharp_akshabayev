﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
    struct Point
    {
        public int x, y;

        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public static Point operator +(Point arg1, int arg2)
        {
            arg1.x += arg2;
            arg1.y += arg2;
            return arg1;
        }

        public static Point operator -(Point arg1, int arg2)
        {
            arg1.x -= arg2;
            arg1.y -= arg2;
            return arg1;
        }

        public void Print()
        {
            Console.WriteLine(x.ToString() + " " + y.ToString());
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(10, 12);
            Console.WriteLine(a);
            Point b = a;
            a = a + 10;
            Console.WriteLine(a);
            b = b - 5;
            Console.WriteLine(a);
            Console.ReadKey();
        }
    }
}
