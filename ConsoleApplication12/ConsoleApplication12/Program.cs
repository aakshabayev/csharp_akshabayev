﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication12
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = @"text 1
            text2 
            text4";

            StringReader sr = new StringReader(s);
            while (sr.Peek() != -1)
            {
                string line = sr.ReadLine();
                Console.WriteLine(line);
            }
            Console.ReadKey();
        }
    }
}
