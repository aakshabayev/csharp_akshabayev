﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace ConsoleApplication14
{
    class Program
    {
        static void Main(string[] args)
        {
            FileStream file = File.Open("c:/asd/c.txt", FileMode.Open);
            BinaryReader br = new BinaryReader(file);

            long number = br.ReadInt64();
            byte[] bytes = br.ReadBytes(4);
            string s = br.ReadString();

            br.Close();

            Console.WriteLine(number);

            foreach (byte b in bytes)
            {
                Console.WriteLine("{2} [{0}], {1}", b, "hello", "world");
            }

            Console.WriteLine();
            Console.WriteLine(s);
            Console.ReadKey();
        }
    }
}
