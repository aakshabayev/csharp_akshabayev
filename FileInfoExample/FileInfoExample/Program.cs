﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileInfoExample
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo file = new FileInfo("c:/asd/a.txt");
            if (file.Exists)
            {
                Console.WriteLine(file.FullName);
                Console.WriteLine(file.Name);
                Console.WriteLine(file.CreationTime);
                Console.WriteLine(file.LastAccessTime);
                file.CopyTo("c:/asd/c.txt");
            }
            else
            {
                Console.WriteLine("error occured");
            }
            Console.ReadKey();
        }
    }
}
