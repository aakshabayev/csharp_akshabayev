﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    struct DNumber
    {
        int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }

        public override string ToString()
        {
            return a.ToString() + "/"+ b.ToString();
        }

        public static DNumber operator *(DNumber x, DNumber y)
        {
            x.a *= y.a;
            x.b *= y.b;
            return x;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            DNumber x = new DNumber(2, 3);
            DNumber y = new DNumber(1, 2);
            DNumber z = x * y;
            DNumber q = x * y;
            Console.WriteLine(z);
            Console.WriteLine(q);
            Console.ReadKey();
        }
    }
}
