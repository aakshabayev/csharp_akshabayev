﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    struct Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

        public static Point operator +(Point arg1, Point arg2)
        {
            arg1.x += arg2.x;
            arg1.y += arg2.y;
            return arg1;
        }

         
    }
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(5, 6);
            Point b = new Point(1, 3);
            Console.WriteLine(a);
            Point c = a + b;
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
