﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ReadTextFileExample1
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "c:/asd/k.txt";
            if (File.Exists(path))
            {
                File.Delete(path);
            }

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine("hello");
                sw.WriteLine("world");
                sw.WriteLine("ttt");
            }

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    Console.WriteLine(sr.ReadLine());
                }
            }

            Console.ReadKey();
        }
    }
}
