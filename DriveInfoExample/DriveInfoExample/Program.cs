﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DriveInfoExample
{
    class Program
    {
        static void Main(string[] args)
        {
            DriveInfo[] drives = DriveInfo.GetDrives();
            foreach (DriveInfo drive in drives)
            {
                Console.WriteLine(drive.Name);
                Console.WriteLine(drive.DriveType);
                if (drive.DriveType != DriveType.CDRom)
                {
                    Console.WriteLine(drive.AvailableFreeSpace / 1024 / 1024);
                }
            }
            Console.ReadKey();
        }
    }
}
