﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintDemo
{
    public partial class Form1 : Form
    {
        Point a, b;
        bool isPressed = false;
        Bitmap bitmap;
        Graphics g_bitmap;

        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(this.Width, this.Height);
            g_bitmap = Graphics.FromImage(bitmap);
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            a = e.Location;
            isPressed = true;
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            isPressed = false;
            int x = Math.Min(a.X, b.X);
            int y = Math.Min(a.Y, b.Y);
            int w = Math.Abs(a.X - b.X);
            int h = Math.Abs(a.Y - b.Y);

            g_bitmap.DrawRectangle(new Pen(Color.Green), x, y, w, h);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isPressed == true)
            {
                b = e.Location;
                Graphics g = this.CreateGraphics();
                int x = Math.Min(a.X, b.X);
                int y = Math.Min(a.Y, b.Y);
                int w = Math.Abs(a.X - b.X);
                int h = Math.Abs(a.Y - b.Y);
                g.Clear(Color.White);
                g.DrawImage(bitmap, 20, 20);
                g.DrawRectangle(new Pen(Color.Blue), x, y, w, h);
            }
        }
    }
}
