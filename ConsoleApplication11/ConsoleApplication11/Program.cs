﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication11
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "c:/asd/a.txt";
            using (StreamReader sr = new StreamReader(path))
            {
                char[] c = null;
                c = new char[15];
                sr.Read(c, 0, c.Length);
                Console.WriteLine(c);

                sr.DiscardBufferedData();
                sr.BaseStream.Seek(2, SeekOrigin.Begin);
                Console.WriteLine(sr.ReadToEnd());
                Console.ReadKey();
            }
        }
    }
}
