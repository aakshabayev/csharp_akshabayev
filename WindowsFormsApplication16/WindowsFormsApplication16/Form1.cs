﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication16
{
    public partial class Form1 : Form
    {
        SeaBattle seab = new SeaBattle();
        MyButton[,] btns = new MyButton[4, 4];
        int k;

        MyButton[,] seabtns = new MyButton[12, 12];

        public Form1()
        {
            InitializeComponent();
        }

        public void btn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            k = x + 1;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    if (i == x)
                    {
                        btns[i, j].BackColor = Color.Red;
                    }
                    else
                        btns[i, j].BackColor = Color.White;
                }
            }

        }

        public void seabtn_click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            if (seab.check(x, y, k, true))
            {
                seab.set(x, y, k, true);
                for (int i = y; i < y + k; i++)
                    seabtns[x, i].BackColor = Color.Red;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(20, 20);
                    btns[i, j].Location = new Point(j * 25, i * 25);
                    btns[i, j].Click += btn_click;
                    this.Controls.Add(btns[i, j]);
                }
            }

            // crating seabattle board
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    seabtns[i, j] = new MyButton(i, j);
                    seabtns[i, j].Size = new Size(45, 45);
                    seabtns[i, j].Location = new Point(10 + j * 46, 110 + i * 46);
                    seabtns[i, j].MouseMove += button_MouseMove;
                    seabtns[i, j].Click += seabtn_click;
                    this.Controls.Add(seabtns[i, j]);

                }
            }
        }

        private void button_MouseMove(object sender, MouseEventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    if (seabtns[i, j].BackColor == Color.Yellow)
                        seabtns[i, j].BackColor = Color.White;
                }
            }

            if (seab.check(x, y, k, true) == true)
            {
                for (int i = y; i < y + k; i++)
                    seabtns[x, i].BackColor = Color.Yellow;
            }
        }
    }
}
